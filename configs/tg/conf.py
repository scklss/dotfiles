import os
PHONE = "xxxx"
NOTIFY_CMD = "/usr/bin/notify-send {subtitle} {msg}"
CHAT_FLAGS = {
    "online": "●",
    "pinned": "P",
    "muted": "M",
    # chat is marked as unread
    "unread": "U",
    # last msg haven't been seen by recipient
    "unseen": "✓",
    "secret": "><",
    "seen": "✓✓",  # leave empty if you don't want to see it
}
MSG_FLAGS = {
    "selected": "*",
    "forwarded": "F",
    "new": "N",
    "unseen": "U",
    "edited": "E",
    "pending": "...",
    "failed": "X",
    "seen": "✓✓",  # leave empty if you don't want to see it
}
KEEP_MEDIA = 2
FILES_DIR = os.path.expanduser("~/Downloads/tg")
MAX_DOWNLOAD_SIZE = "0MB"
