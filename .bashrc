# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Vi mode
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

# Aliases
alias ls='ls -lah --color=auto'
alias nv='nvim'
alias ga='git add .'
alias gs='git status'
alias gc='git commit -m'
alias gps='git push'
alias gpl='git pull'
alias vc='sudo vmware-modconfig --console --install-all'

# PS1="\[\033[0;34m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;34m\]]\342\224\200\")[$(if [[ ${EUID} == 0
# ]]; then echo '\[\033[01;31m\]root\[\033[01;33m\]@\[\033[01;96m\]\h'; else echo '\[\033[0;32m\]\u\[\033[01;33m\]@\[\033[01;96m\]\h'; fi)\[\033[0;34m\]]\342\224\200[\[\033[0;32m\]\w\[\033[0;34m\]]\n\[\033[0;34m\]\342\224\224\342\224\200\342\224\200 \[\033[0m\]\[\e[01;33m\]\\$ \[\e[0m\]"

PS1="\[\033[0;34m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;34m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]root\[\033[01;33m\]@\[\033[01;96m\]\h'; else echo '\[\033[01;36m\]\u\[\033[01;33m\]@\[\033[01;33m\]\h'; fi)\[\033[0;34m\]]\342\224\200[\[\033[0;32m\]\w\[\033[0;34m\]]\n\[\033[0;34m\]\342\224\224\342\224\200\342\224\200 \[\033[0m\]\[\e[01;33m\]\\$ \[\e[0m\]"


# PS1='\u@\h:\w\$ '

# grep color
export GREP_COLORS='ms=01;41'

# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Scripts

if [ -d $HOME/dwm_scripts ]; then
        PATH=$HOME/dwm_scripts:$PATH
fi


if [ -d $HOME/sxhkd_scripts ]; then
        PATH=$HOME/sxhkd_scripts:$PATH
fi

if [ -d $HOME/.local/bin ]; then
        PATH=$HOME/.local/bin:$PATH
fi

export EDITOR=nvim

source /usr/share/doc/fzf/key-bindings.bash
source /usr/share/doc/fzf/completion.bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
