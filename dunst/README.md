# Install Packages
```
sudo apt install dunst libnotify-bin
```

# Add config
- Create dunst config directory
	```
	mkdir ~/.config/dunst
	```
- Copy the config file
	```
	cp /path/to/dotfiles/dunst/dunstrc ~/.config/dunst/dunstrc
	```
