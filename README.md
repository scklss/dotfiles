# For Ubuntu Server 20.04
### Remove snap - [Source](https://askubuntu.com/questions/1280707/how-to-uninstall-snap)
- Stop snap services
    ```
    sudo systemctl stop snapd && sudo systemctl disable snapd
    sudo apt purge snapd
    rm -rf ~/snap
    sudo rm -rf /snap /var/snap /var/lib/snapd /var/cache/snapd /usr/lib/snapd
    ```
- Stop snap update create a nosnap.pref file (`sudo -H vim /etc/apt/preferences.d/no-snap.pref`) with following content
    ```
    # To install snapd, specify its version with 'apt install snapd=VERSION'
    # where VERSION is the version of the snapd package you want to install.
    Package: snapd
    Pin: release a=*
    Pin-Priority: -10
    ```
### Install packages
```
sudo apt install xserver-xorg-core x11-xserver-utils build-essential libx11-dev libxft-dev libxinerama-dev fonts-font-awesome pulseaudio xwallpaper xcompmgr brightnessctl
```

### Update .bashrc for putting dwm_scripts & sxhkd_scripts in $PATH
```
if [ -d "$HOME/dwm_scripts" ] ; then
    PATH="$HOME/dwm_scripts:$PATH"
fi

if [ -d "$HOME/sxhkd_scripts" ] ; then
    PATH="$HOME/sxhkd_scripts:$PATH"
fi
```

### Permission for brightness change
- For permission add user to video group (`sudo usermod -aG video <username>`).
- Then `logout` and log back in to take effect.

### Enable touchpad tap-to-click
- Check if the directory `/etc/X11/xorg.conf.d/` exists or not.
- If it exists read [here](https://wiki.archlinux.org/index.php/Libinput#Via_Xorg_configuration_file)
- If it does not exist create the directory (`sudo mkdir /etc/X11/xorg.conf.d/`), and create a touchpad.conf file (`sudo vim /etc/X11/xorg.conf.d/30-touchpad.conf`) with the following content ([source](https://superuser.com/a/1528216))
    ```
    Section "InputClass"
    	Identifier "touchpad catchall"
    	Driver "libinput"
    	Option "Tapping" "on"
    EndSection
    ```
- Restart Xorg.

# For Void
### Install packages
```
sudo xbps-install xorg-minimal xorg-fonts font-awesome5 libXft-devel libX11-devel libXinerama-devel NetworkManager brightnessctl xwallpaper xrandr xrdb pulseaudio base-devel xcompmgr gst-libav
```

### User groups
```
sudo usermod -aG wheel,audio,video
```

### Firefox font issue
- Remove Helvetica fonts downloaded along with xorg-fonts, `sudo rm /usr/share/fonts/X11/*/helv*`

# Qemu
- [Source](https://drewdevault.com/2018/09/10/Getting-started-with-qemu.html)
### Create the VM file
- `qemu-img create -f qcow2 <name of the vm> <hdd size, e.g.,20G>`

*Explanations*
- qcow2 format being a format which appears to be 20G(in this case) to the guest (VM), but only actually writes to the host any sectors which were written to by the guest in practice. You can also expose this as a block device on your local system (or a remote system!) with qemu-nbd if you need to.

### Add iso, memory size and others
```
qemu-system-x86_64 \
    -enable-kvm \
    -m 2048 \
    -nic user,model=virtio \
    -drive file=<path to vm>,media=disk,if=virtio \
    -cdrom <path to iso> \
    -sdl
```

*Explanations*
- `-enable-kvm`: This enables use of the KVM (kernel virtual machine) subsystem to use hardware accelerated virtualisation on Linux hosts.
- `-m 2048`: This specifies 2048M (2G) of RAM to provide to the guest.
- `-nic user,model=virtio`: Adds a virtual network interface controller, using a virtual LAN emulated by qemu. This is the most straightforward way to get internet in a guest, but there are other options (for example, you will probably want to use -nic tap if you want the guest to do networking directly on the host NIC). model=virtio specifies a special virtio NIC model, which is used by the virtio kernel module in the guest to provide faster networking.
- `-drive file=<path to vm>,media=disk,if=virtio`: This attaches our virtual disk to the guest. It’ll show up as /dev/vda. We specify if=virtio for the same reason we did for -nic: it’s the fastest interface, but requires special guest support from the Linux virtio kernel module.
- `-cdrom <path to iso>` connects a virtual CD drive to the guest and loads our install media into it.
- `-sdl` finally specifies the graphical configuration. We’re using the SDL backend, which is the simplest usable graphical backend. It attaches a display to the guest and shows it in an [SDL](https://www.libsdl.org/) window on the host.

### Run post installation
- Same command as above without the -cdrom.
```
qemu-system-x86_64 \
    -enable-kvm \
    -m 2048 \
    -nic user,model=virtio \
    -drive file=<path to vm>,media=disk,if=virtio \
    -sdl
```
### Other stuff
- Using `-spice` instead of `-sdl` to enable remote access to the display/keyboard/mouse
- Read-only disk images with guest writes stored in RAM (`snapshot=on`)
- Non-graphical boot with `-nographic` and `console=ttyS0` configured in your kernel command line
- Giving a genuine graphics card to your guest with KVM passthrough for high performance gaming, OpenCL, etc
- Using [virt-manager](https://virt-manager.org/) or [Boxes](https://help.gnome.org/users/gnome-boxes/stable/) if you want a GUI to hold your hand


